﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.ManagedDataAccess.Types;

namespace databasesprojectCAT
{
    public partial class productForm1 : Form
    {
        string oradb;
        OracleConnection conn;

        public productForm1()
        {
            InitializeComponent();
        }

        private void productForm1_Load(object sender, EventArgs e)
        {
            oradb = "Data Source=(DESCRIPTION =" + "(ADDRESS = (PROTOCOL = TCP)(HOST = oracle.cms.waikato.ac.nz)(PORT = 1521))" + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = teaching.cms.waikato.ac.nz)));" + "User Id= COMP329_04;Password=JtT2XhBE69;";
            conn = new OracleConnection(oradb);
            conn.Open();
            populateProductDisplay();
        }

        private void searchCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void searchInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void searchButton_Click(object sender, EventArgs e)
        {

        }

        private void sortBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void productDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void populateProductDisplay()
        {
            OracleCommand populateProductsQuery = new OracleCommand();
            populateProductsQuery.Connection = conn;
            populateProductsQuery.CommandText = "select * from PRODUCTS order by TITLE asc";
            populateProductsQuery.CommandType = CommandType.Text;
            OracleDataReader dr = populateProductsQuery.ExecuteReader();
            while (dr.Read())
            {

                productDisplay.Items.Add(dr.GetInt32(0));
                productDisplay1.Items.Add(dr.GetString(1));
                productDisplay2.Items.Add(dr.GetInt32(2));
                //productDisplay3.Items.Add(dr.GetInt32(0));
                productDisplay4.Items.Add(dr.GetString(4));
                productDisplay5.Items.Add(dr.GetString(5));
                productDisplay6.Items.Add(dr.GetInt32(6));

            };

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
