﻿namespace databasesprojectCAT
{
    partial class productForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.productDisplay = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchCatagory = new System.Windows.Forms.ComboBox();
            this.searchInput = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.sortBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.productDisplay1 = new System.Windows.Forms.ListBox();
            this.productDisplay2 = new System.Windows.Forms.ListBox();
            this.productDisplay4 = new System.Windows.Forms.ListBox();
            this.productDisplay5 = new System.Windows.Forms.ListBox();
            this.productDisplay6 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(330, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 67);
            this.label1.TabIndex = 0;
            this.label1.Text = "Products";
            // 
            // productDisplay
            // 
            this.productDisplay.ColumnWidth = 111;
            this.productDisplay.FormattingEnabled = true;
            this.productDisplay.Location = new System.Drawing.Point(200, 198);
            this.productDisplay.MultiColumn = true;
            this.productDisplay.Name = "productDisplay";
            this.productDisplay.Size = new System.Drawing.Size(23, 342);
            this.productDisplay.TabIndex = 1;
            this.productDisplay.SelectedIndexChanged += new System.EventHandler(this.productDisplay_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sortBox);
            this.groupBox1.Controls.Add(this.searchButton);
            this.groupBox1.Controls.Add(this.searchInput);
            this.groupBox1.Controls.Add(this.searchCatagory);
            this.groupBox1.Location = new System.Drawing.Point(200, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 89);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Refine";
            // 
            // searchCatagory
            // 
            this.searchCatagory.FormattingEnabled = true;
            this.searchCatagory.Items.AddRange(new object[] {
            "ID",
            "TITLE",
            "PRICE",
            "PLATFORM",
            "PUBLISHER",
            "QUANTITY"});
            this.searchCatagory.Location = new System.Drawing.Point(4, 37);
            this.searchCatagory.Name = "searchCatagory";
            this.searchCatagory.Size = new System.Drawing.Size(121, 21);
            this.searchCatagory.TabIndex = 0;
            this.searchCatagory.SelectedIndexChanged += new System.EventHandler(this.searchCatagory_SelectedIndexChanged);
            // 
            // searchInput
            // 
            this.searchInput.Location = new System.Drawing.Point(4, 64);
            this.searchInput.Name = "searchInput";
            this.searchInput.Size = new System.Drawing.Size(121, 20);
            this.searchInput.TabIndex = 1;
            this.searchInput.TextChanged += new System.EventHandler(this.searchInput_TextChanged);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(131, 63);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 22);
            this.searchButton.TabIndex = 2;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // sortBox
            // 
            this.sortBox.FormattingEnabled = true;
            this.sortBox.Items.AddRange(new object[] {
            "ID",
            "TITLE",
            "PRICE",
            "PLATFORM",
            "PUBLISHER",
            "QUANTITY"});
            this.sortBox.Location = new System.Drawing.Point(328, 37);
            this.sortBox.Name = "sortBox";
            this.sortBox.Size = new System.Drawing.Size(121, 21);
            this.sortBox.TabIndex = 3;
            this.sortBox.SelectedIndexChanged += new System.EventHandler(this.sortBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(325, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Sort by:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // productDisplay1
            // 
            this.productDisplay1.ColumnWidth = 111;
            this.productDisplay1.FormattingEnabled = true;
            this.productDisplay1.Location = new System.Drawing.Point(219, 198);
            this.productDisplay1.MultiColumn = true;
            this.productDisplay1.Name = "productDisplay1";
            this.productDisplay1.Size = new System.Drawing.Size(183, 342);
            this.productDisplay1.TabIndex = 3;
            // 
            // productDisplay2
            // 
            this.productDisplay2.ColumnWidth = 111;
            this.productDisplay2.FormattingEnabled = true;
            this.productDisplay2.Location = new System.Drawing.Point(401, 198);
            this.productDisplay2.MultiColumn = true;
            this.productDisplay2.Name = "productDisplay2";
            this.productDisplay2.Size = new System.Drawing.Size(28, 342);
            this.productDisplay2.TabIndex = 4;
            // 
            // productDisplay4
            // 
            this.productDisplay4.ColumnWidth = 111;
            this.productDisplay4.FormattingEnabled = true;
            this.productDisplay4.Location = new System.Drawing.Point(428, 198);
            this.productDisplay4.MultiColumn = true;
            this.productDisplay4.Name = "productDisplay4";
            this.productDisplay4.Size = new System.Drawing.Size(79, 342);
            this.productDisplay4.TabIndex = 6;
            // 
            // productDisplay5
            // 
            this.productDisplay5.ColumnWidth = 111;
            this.productDisplay5.FormattingEnabled = true;
            this.productDisplay5.Location = new System.Drawing.Point(506, 198);
            this.productDisplay5.MultiColumn = true;
            this.productDisplay5.Name = "productDisplay5";
            this.productDisplay5.Size = new System.Drawing.Size(147, 342);
            this.productDisplay5.TabIndex = 7;
            // 
            // productDisplay6
            // 
            this.productDisplay6.ColumnWidth = 111;
            this.productDisplay6.FormattingEnabled = true;
            this.productDisplay6.Location = new System.Drawing.Point(652, 198);
            this.productDisplay6.MultiColumn = true;
            this.productDisplay6.Name = "productDisplay6";
            this.productDisplay6.Size = new System.Drawing.Size(23, 342);
            this.productDisplay6.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(197, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "ID";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(216, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Title";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(398, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Price";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label6.Location = new System.Drawing.Point(425, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Platform";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(506, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Publisher";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(649, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Qty";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Search by:";
            // 
            // productForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.productDisplay6);
            this.Controls.Add(this.productDisplay5);
            this.Controls.Add(this.productDisplay4);
            this.Controls.Add(this.productDisplay2);
            this.Controls.Add(this.productDisplay1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.productDisplay);
            this.Controls.Add(this.label1);
            this.Name = "productForm1";
            this.Text = "productForm1";
            this.Load += new System.EventHandler(this.productForm1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox productDisplay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox sortBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TextBox searchInput;
        private System.Windows.Forms.ComboBox searchCatagory;
        private System.Windows.Forms.ListBox productDisplay1;
        private System.Windows.Forms.ListBox productDisplay2;
        private System.Windows.Forms.ListBox productDisplay4;
        private System.Windows.Forms.ListBox productDisplay5;
        private System.Windows.Forms.ListBox productDisplay6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}