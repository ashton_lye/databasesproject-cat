﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesprojectCAT
{
    public partial class navigationForm : Form
    {
        public navigationForm()
        {
            InitializeComponent();
        }

        private void customerButton_Click(object sender, EventArgs e)
        {
            var customerForm = new customerForm();
            this.Hide();
            customerForm.ShowDialog();
            this.Close();
        }
    }
}
