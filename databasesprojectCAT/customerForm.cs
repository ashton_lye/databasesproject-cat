﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client; // ODP.NET Oracle managed provider
using Oracle.ManagedDataAccess.Types;

namespace databasesprojectCAT
{
    public partial class customerForm : Form
    {
        //instantiate variables here so they can be used by all functions
        string oradb;
        OracleConnection conn;
        //create a list that stores the customer id's returned by the queries - can use this to get customer id from listBox index
        List<int> customerIDList = new List<int>();
        
        public customerForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //set values for connection string and create/open connection
            oradb = "Data Source=(DESCRIPTION =" + "(ADDRESS = (PROTOCOL = TCP)(HOST = oracle.cms.waikato.ac.nz)(PORT = 1521))" + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = teaching.cms.waikato.ac.nz)));" + "User Id= COMP329_04;Password=JtT2XhBE69;";
            conn = new OracleConnection(oradb);
            conn.Open();

            //call the function that populates the listbox with all customers
            showAllCustomersButton_Click(sender, e);

            //populate the 'search by' combo box with the columns of the table
            OracleCommand populateComboBox = new OracleCommand();
            populateComboBox.Connection = conn;
            populateComboBox.CommandText = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME = 'CUSTOMERS'";
            populateComboBox.CommandType = CommandType.Text;
            OracleDataReader dr1 = populateComboBox.ExecuteReader();
            while (dr1.Read())
            {
                searchByComboBox.Items.Add(dr1.GetString(0));
            }
        }

        private void customerListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get the customer id by comparing it's list index with the selected index from the listBox
            //customer's name index in the listBox is the same as their id's index in the list
            var selectedCustomerID = customerIDList[customerListBox.SelectedIndex];

            //execute a query to get the customer's details and display them
            OracleCommand detailsQuery = new OracleCommand();
            detailsQuery.Connection = conn;
            detailsQuery.CommandText = "select * from CUSTOMERS WHERE memberID = " + selectedCustomerID;
            detailsQuery.CommandType = CommandType.Text;
            OracleDataReader dr = detailsQuery.ExecuteReader();
            while (dr.Read())
            {
                memNoLabel.Text = dr.GetInt32(0).ToString();
                fnameLabel.Text = dr.GetString(1);
                lnameLabel.Text = dr.GetString(2);
                emailLabel.Text = dr.GetString(3);
                phoneLabel.Text = dr.GetString(4);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            //get the search parameters
            string searchColumn = searchByComboBox.SelectedItem.ToString();
            string searchTerm = searchTextBox.Text;

            //clear the listBox and the id list so we can put the search results in them
            customerIDList.Clear();
            customerListBox.Items.Clear();

            //execute search query using the parameters chosen by the user, and display the results in the listBox
            OracleCommand searchQuery = new OracleCommand();
            searchQuery.Connection = conn;
            searchQuery.CommandText = "select MEMBERID, FNAME, LNAME from CUSTOMERS where "+ searchColumn +" LIKE '"+ searchTerm +"' order by MEMBERID asc";
            searchQuery.CommandType = CommandType.Text;
            OracleDataReader dr = searchQuery.ExecuteReader();
            while (dr.Read())
            {
                customerIDList.Add(dr.GetInt32(0));
                customerListBox.Items.Add(dr.GetString(1) + " " + dr.GetString(2));
            }
        }

        private void showAllCustomersButton_Click(object sender, EventArgs e)
        {
            //clear listBox and id list
            customerIDList.Clear();
            customerListBox.Items.Clear();

            //get all customers id's and names to display in the listBox
            OracleCommand populateListQuery = new OracleCommand();
            populateListQuery.Connection = conn;
            populateListQuery.CommandText = "select MEMBERID, FNAME, LNAME from CUSTOMERS order by MEMBERID asc";
            populateListQuery.CommandType = CommandType.Text;
            OracleDataReader dr = populateListQuery.ExecuteReader();
            while (dr.Read())
            {
                customerIDList.Add(dr.GetInt32(0));
                customerListBox.Items.Add(dr.GetString(1) + " " + dr.GetString(2));
            };
        }

        private void createCustomerButton_Click(object sender, EventArgs e)
        {
            fnameTextBox.Show();
            lnameTextBox.Show();
            emailTextBox.Show();
            phoneTextBox.Show();
            submitDetailsButton.Show();
            cancelButton.Show();
            createCustomerButton.Hide();
            showAllCustomersButton.Hide();
            editCustomerButton.Hide();
            deleteCustomerButton.Hide();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            fnameTextBox.Hide();
            fnameTextBox.Text = "";
            lnameTextBox.Hide();
            lnameTextBox.Text = "";
            emailTextBox.Hide();
            emailTextBox.Text = "";
            phoneTextBox.Hide();
            phoneTextBox.Text = "";
            submitDetailsButton.Hide();
            cancelButton.Hide();
            createCustomerButton.Show();
            showAllCustomersButton.Show();
            editCustomerButton.Show();
            deleteCustomerButton.Show();
        }

        private void submitDetailsButton_Click(object sender, EventArgs e)
        {
            var fname = fnameTextBox.Text.Trim();
            var lname = lnameTextBox.Text.Trim();
            var email = emailTextBox.Text.Trim();
            var phone = phoneTextBox.Text.Trim();

            if (fname == "" || lname == "" || email == "" || phone == "")
            {
                Console.WriteLine("dont leave ya shit blank boi");
            } else
            {
                Console.WriteLine("insert into CUSTOMERS values (CUSTOMERSEQUENCE.NEXTVAL, " + fname + ", " + lname + ", " + email + ", " + phone + ")");
               
            }
        }
    }
}
