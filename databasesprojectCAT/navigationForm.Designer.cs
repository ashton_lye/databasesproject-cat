﻿namespace databasesprojectCAT
{
    partial class navigationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.customerButton = new System.Windows.Forms.Button();
            this.transactionButton = new System.Windows.Forms.Button();
            this.stockButton = new System.Windows.Forms.Button();
            this.staffButton = new System.Windows.Forms.Button();
            this.orderButton = new System.Windows.Forms.Button();
            this.supplierButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(265, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(354, 55);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Function";
            // 
            // customerButton
            // 
            this.customerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerButton.Location = new System.Drawing.Point(80, 162);
            this.customerButton.Name = "customerButton";
            this.customerButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.customerButton.Size = new System.Drawing.Size(196, 76);
            this.customerButton.TabIndex = 3;
            this.customerButton.Text = "Customer Accounts";
            this.customerButton.UseVisualStyleBackColor = true;
            this.customerButton.Click += new System.EventHandler(this.customerButton_Click);
            // 
            // transactionButton
            // 
            this.transactionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transactionButton.Location = new System.Drawing.Point(344, 162);
            this.transactionButton.Name = "transactionButton";
            this.transactionButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.transactionButton.Size = new System.Drawing.Size(196, 76);
            this.transactionButton.TabIndex = 4;
            this.transactionButton.Text = "Make Transaction";
            this.transactionButton.UseVisualStyleBackColor = true;
            // 
            // stockButton
            // 
            this.stockButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockButton.Location = new System.Drawing.Point(608, 162);
            this.stockButton.Name = "stockButton";
            this.stockButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.stockButton.Size = new System.Drawing.Size(196, 76);
            this.stockButton.TabIndex = 5;
            this.stockButton.Text = "View Stock";
            this.stockButton.UseVisualStyleBackColor = true;
            // 
            // staffButton
            // 
            this.staffButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staffButton.Location = new System.Drawing.Point(80, 322);
            this.staffButton.Name = "staffButton";
            this.staffButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.staffButton.Size = new System.Drawing.Size(196, 76);
            this.staffButton.TabIndex = 6;
            this.staffButton.Text = "Staff Functions";
            this.staffButton.UseVisualStyleBackColor = true;
            // 
            // orderButton
            // 
            this.orderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderButton.Location = new System.Drawing.Point(344, 322);
            this.orderButton.Name = "orderButton";
            this.orderButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.orderButton.Size = new System.Drawing.Size(196, 76);
            this.orderButton.TabIndex = 7;
            this.orderButton.Text = "Order Stock";
            this.orderButton.UseVisualStyleBackColor = true;
            // 
            // supplierButton
            // 
            this.supplierButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierButton.Location = new System.Drawing.Point(608, 322);
            this.supplierButton.Name = "supplierButton";
            this.supplierButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.supplierButton.Size = new System.Drawing.Size(196, 76);
            this.supplierButton.TabIndex = 8;
            this.supplierButton.Text = "Supplier Info";
            this.supplierButton.UseVisualStyleBackColor = true;
            // 
            // navigationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.supplierButton);
            this.Controls.Add(this.orderButton);
            this.Controls.Add(this.staffButton);
            this.Controls.Add(this.stockButton);
            this.Controls.Add(this.transactionButton);
            this.Controls.Add(this.customerButton);
            this.Controls.Add(this.label1);
            this.Name = "navigationForm";
            this.Text = "navigationForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button customerButton;
        private System.Windows.Forms.Button transactionButton;
        private System.Windows.Forms.Button stockButton;
        private System.Windows.Forms.Button staffButton;
        private System.Windows.Forms.Button orderButton;
        private System.Windows.Forms.Button supplierButton;
    }
}